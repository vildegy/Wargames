package no.ntnu.vildegy.wargames.model;

public class InfantryUnit extends Unit {

    /**
     * Constructor, initializing a new InfantryUnit
     * Inherit from the superclass Unit
     *
     * @param name   a short descriptive name
     * @param health value of health, cannot be less than 0
     * @param attack represents the unit´s weapon
     * @param armour defensive value that protects during attack
     */
    public InfantryUnit(String name, int health, int attack, int armour) {

        super(name, health, attack, armour);
    }

    public InfantryUnit(String name, int health) {
        super(name, health, 15, 10);

    }

    @Override
    public int getAttackBonus(Terrain terrain) {
        int meleeBonus = 2;
        if(terrain == Terrain.FOREST){
            meleeBonus += 3;
        }

        return meleeBonus;
    }

    @Override
    public int getResistBonus(Terrain terrain) {
        int defenseBonus = 1;
        if(terrain == Terrain.FOREST) {
            defenseBonus += 3;
        }
        return defenseBonus;
    }


}
