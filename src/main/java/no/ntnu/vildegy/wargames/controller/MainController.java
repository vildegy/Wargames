package no.ntnu.vildegy.wargames.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import no.ntnu.vildegy.wargames.filehandler.ExportUnits;
import no.ntnu.vildegy.wargames.filehandler.ImportUnits;
import no.ntnu.vildegy.wargames.model.Army;
import no.ntnu.vildegy.wargames.model.Unit;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Scanner;

public class MainController {

    private Army army;

    @FXML
    private ObservableList<Unit> observableList;

    @FXML
    public TableColumn<Army,String> armyOneColumn;
    @FXML
    public TableColumn<Army,String> armyTwoColumn;
    @FXML
    public TableColumn<Army,String> winnerColumn;
    @FXML
    public TableView<Unit> tableView;
    public Button clearButton;
    public Button simulateButton;
    @FXML
    public AnchorPane anchorPane;
    public MenuBar menuBar;



    /**
     * Initializing the application
     * creates an instance of Army and an
     * observable list to show in tableview
     */
    @FXML
    public void initialize() throws IOException {
        armyOneColumn.setCellValueFactory(new PropertyValueFactory<>("ARMY_ONE"));
        winnerColumn.setCellValueFactory(new PropertyValueFactory<>("WINNER"));
        armyTwoColumn.setCellValueFactory(new PropertyValueFactory<>("ARMY_TWO"));

        anchorPane.setStyle("-fx-background-color: #424242");
        this.observableList = FXCollections.observableArrayList();


    }

    /**
     * Shows an about box with information of the program
     */
    @FXML
    public void showAboutDialog() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog - About");
        alert.setContentText("Vilde Gylterud" + "\n" + "Version: 1.0-SNAPSHOT" );

        //To get the dark mode design on all the dialog boxes
        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.setStyle("-fx-background-color: #424242; -fx-prompt-text-fill: white; -fx-base: #121212; -fx-focus-traversable: false;  -fx-control-inner-background: derive(-fx-base, 35%);\n" +
                "    -fx-control-inner-background-alt: -fx-control-inner-background ; -fx-accent: #212121;\n" +
                "    -fx-focus-color: -fx-accent;");

        alert.showAndWait();
    }


    /**
     * Method for exit the application
     * Asks the user if she really wants to exit the application
     */
    @FXML
    public void exitApplication() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm");
        alert.setHeaderText("Exit?");
        alert.setContentText("Are you sure you want to exit?");

        //To get the dark mode design on the dialog boxes too
        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.setStyle("-fx-background-color: #424242; -fx-prompt-text-fill: white; -fx-base: #121212; -fx-focus-traversable: false;  -fx-control-inner-background: derive(-fx-base, 35%);\n" +
                "    -fx-control-inner-background-alt: -fx-control-inner-background ; -fx-accent: #212121;\n" +
                "    -fx-focus-color: -fx-accent;");

        Optional<ButtonType> result = alert.showAndWait();

        if (result.isPresent() && (result.get() == ButtonType.OK)) {

            Platform.exit();
        }
    }

    /** Method that sends a selected message to the user
     *
     * @param message selected message to the user
     */
    public void messageAlertToUser(String message) {
        Alert alert = new Alert((Alert.AlertType.INFORMATION));

        alert.setTitle("Information");
        alert.setContentText(message);

        //To get the dark mode design on all the dialog boxes
        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.setStyle("-fx-background-color: #424242; -fx-prompt-text-fill: white; -fx-base: #121212; -fx-focus-traversable: false;  -fx-control-inner-background: derive(-fx-base, 35%);\n" +
                "    -fx-control-inner-background-alt: -fx-control-inner-background ; -fx-accent: #212121;\n" +
                "    -fx-focus-color: -fx-accent;");

        alert.showAndWait();
    }

    /** Method that clears the tableview
     *  and sets/adds the new values in it
     *
     * @param arrayList a list with all the postal codes
     */
    public void updateTableView(ArrayList<Unit> arrayList) {
        tableView.getItems().clear();
        observableList.setAll(arrayList);
        tableView.getItems().addAll(observableList);
    }

    public void clearTables() {

    }



    @FXML
    private void simulate() {

    }


    /**
     * Method for exporting Txt file
     */
    @FXML
    public void exportToFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("Csv files (.csv)", ".csv"));
        File importFile = fileChooser.showSaveDialog(null);

        try {
            ExportUnits.exportToFile(this.army, importFile);
            updateTableView(army.getAllUnits());
        } catch (IOException e) {
            messageAlertToUser("Error in exporting file. Could not import file");
            e.printStackTrace();
        }

}

        @FXML
        public void importFromFile() throws IOException, IllegalArgumentException, ArrayIndexOutOfBoundsException {

            FileChooser fileChooser = new FileChooser();
            File file = new File(fileChooser.showOpenDialog(new Stage()).getAbsolutePath());
            Scanner scanner = new Scanner(new File(file.getAbsolutePath()));
            ImportUnits.importArmy(file.getName());


            updateTableView(army.getAllUnits());



        }
}
