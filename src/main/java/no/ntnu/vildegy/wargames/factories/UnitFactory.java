package no.ntnu.vildegy.wargames.factories;

import no.ntnu.vildegy.wargames.model.*;

import java.util.ArrayList;
import java.util.List;

public class UnitFactory {

    /**
     * Creates a new Unit instance according to the given unit type
     *
     * @param unitType The name of the type of the Unit. This name must be a name of a subclass of the Unit class.
     * @param name     The name of the unit.
     * @param health   The health of the unit.
     * @return A unit created by the given parameters. The unit is created using the constructor of the subclass with
     * the name that matches the supplied unitType.
     */
    public Unit makeUnit(String unitType, String name, int health) throws IllegalArgumentException {
        Unit unit;
        switch (unitType) {
            case "CavarlyUnit":
                unit = new CavarlyUnit(name, health);
                break;
            case "CommanderUnit":
                unit = new CommanderUnit(name, health);
                break;
            case "InfantryUnit":
                unit = new InfantryUnit(name, health);
                break;
            case "RangedUnit":
                unit = new RangedUnit(name, health);
                break;
            default:
                throw new IllegalArgumentException("Could not read classname of object using '" + unitType + "'");
        }
        return unit;
    }

    public List<Unit> listOfUnits(int numberOfUnits, String unitType, String nameOfUnit, int healthOfUnit) throws IllegalArgumentException{
        ArrayList<Unit> list = new ArrayList<>();
        for (int i = 0; i < numberOfUnits; i++) {
            list.add(makeUnit(unitType, nameOfUnit, healthOfUnit));
        }
        return list;
    }
}



