package no.ntnu.vildegy.wargames.io.Units;

import no.ntnu.vildegy.wargames.model.CavarlyUnit;
import no.ntnu.vildegy.wargames.model.RangedUnit;
import no.ntnu.vildegy.wargames.model.Terrain;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class CavarlyUnitTest {
    

    @Test
    void getAttackBonusSuccess1() {
        RangedUnit rangedUnit = new RangedUnit("RangedUnit", 100);
        CavarlyUnit cavarlyUnit = new CavarlyUnit("CavarlyUnit", 100);

        rangedUnit.attack(cavarlyUnit, Terrain.PLAINS);

        assertTrue(cavarlyUnit.getAttackBonus(Terrain.PLAINS) == 6);
    }

    @Test
    void getAttackBonusSuccess2() {
        RangedUnit rangedUnit = new RangedUnit("RangedUnit", 100);
        CavarlyUnit cavarlyUnit = new CavarlyUnit("CavarlyUnit", 100);

        rangedUnit.attack(cavarlyUnit, Terrain.PLAINS);
        rangedUnit.attack(cavarlyUnit, Terrain.PLAINS);

        int value = cavarlyUnit.timesAttacked;
        System.out.println(value);

        assertTrue(cavarlyUnit.getAttackBonus(Terrain.PLAINS) == 2);
    }

    @Test
    void getAttackBonusUnSuccess() {
        RangedUnit rangedUnit = new RangedUnit("RangedUnit", 100);
        CavarlyUnit cavarlyUnit = new CavarlyUnit("CavarlyUnit", 100);

        rangedUnit.attack(cavarlyUnit, Terrain.PLAINS);
        rangedUnit.attack(cavarlyUnit, Terrain.PLAINS);

        assertFalse(cavarlyUnit.getAttackBonus(Terrain.PLAINS) == 6);
    }
}
